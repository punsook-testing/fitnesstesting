***Settings***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page1}        http://localhost:5173/classbooking
${URL_page2}        http://localhost:5173/classview/1    
${web_browser}      Edge
${btn_booking}      xpath=//*[@id="app"]/main/div/main/div/div/div[1]/div/div[3]/div[2]/button
${btn_full}         xpath=//*[@id="app"]/main/div/main/div/div/div[3]/div/div[3]/div[2]/button
${dialog_full}      xpath=/html/body/div[2]/div/div[2]/div
      
***Test Cases***
TC-FNS-03-01-01 ตรวจสอบการเลือกวันที่

TC-FNS-03-02-01 ตรวจสอบการทำงานของปุ่มจอง
    Open Browser        ${URL_page1}
    Click Element       ${btn_booking}
    Location Should Be  ${URL_page2}

TC-FNS-03-02-02 ตรวจสอบการทำงานของปุ่มเต็ม
    Open Browser        ${URL_page1}
    Click Button        ${btn_full}
    Element Should Be Visible       ${dialog_full}