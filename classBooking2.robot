***Settings***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page1}        http://localhost:5173/classbooking
${URL_page2}        http://localhost:5173/classview/1    
${web_browser}      edge
${btn_booking}      xpath=//*[@id="app"]/main/div/main/div/div/div[1]/div/div[3]/div[2]/button
${btn_book}         xpath=//*[@id="1"]/main/div[3]/button
${btn_back}         xpath=//*[@id="1"]/main/div[3]/a
${dialog_confirm}   xpath=/html/body/div[2]/div/div[2]/div
${btn_confirm}      xpath=/html/body/div[2]/div/div[2]/div/div[3]/button[2]     
${dialog_close}     xpath=/html/body/div[2]/div/div[2]/div
${btn_close}        xpath=/html/body/div[2]/div/div[2]/div/div[2]/div/button
${btn_cancel}       xpath=/html/body/div[2]/div/div[2]/div/div[3]/button[1]

***Keywords***
ClickBook
    [Documentation]     คลิกปุ่มจองแล้วเปิด dialog
    Open Browser        ${URL_page2}
    Click Element       ${btn_Book}
    Element Should Be Visible       ${dialog_confirm}

***Test Cases***
TC-FNS-03-03-01 ตรวจสอบการแสดงผลรายละเอียดข้อมูลของคลาสที่เลือก
    Open Browser        ${URL_page2}

TC-FNS-03-04-01 ตรวจสอบการทำงานของปุ่ม back
    Open Browser        ${URL_page2}
    Click Element       ${btn_back}
    Location Should Be  ${URL_page1}

TC-FNS-03-04-02 ตรวจสอบการทำงานของปุ่ม book
    ClickBook

TC-FNS-03-05-01 ตรวจสอบการทำงานของปุ่ม confirm
    Open Browser        ${URL_page1}
    Click Button        ${btn_booking}
    Location Should Be  ${URL_page2}
    ClickBook
    Click Element       ${btn_confirm}
    Element Should Be Visible       ${dialog_close}
    Click Element       ${btn_close}
    Location Should Be  ${URL_page1}

TC-FNS-03-05-02 ตรวจสอบการทำงานของปุ่ม cancel
    Open Browser        ${URL_page1}
    Click Button        ${btn_booking}
    Location Should Be  ${URL_page2}
    ClickBook
    Click Element       ${btn_cancel}
    Location Should Be  ${URL_page2}       